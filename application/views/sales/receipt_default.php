<?php 
	// Temporarily loads the system language for to print receipt in the system language rather than user defined.
	load_language(TRUE,array('customers','sales','employees'));
?>

		<div class="main col-md-12 col-sm-12 col-xs-12 fleft">
			<div class="header col-md-12 fleft">
				<div class="left fleft">
					<hr class="hr">
				</div>
				<div class="fleft">
					<div></div>
					<div class="desk">
						<i class="fa fa-desktop fa-2x" aria-hidden="true"></i>
					</div>
				</div>
				<div class="right fleft">
					<hr class="hr">
				</div>
			</div>
			<div class="content col-md-12 fleft">
				<div class="col-md-6 fleft">
					<img src="receipt.png" width="75%;">
				</div>
				<div class="col-md-6 fleft logo">
					<?php
					if($this->config->item('receipt_show_company_name'))
					{
					?>
					<div class="col-md-6 fleft tleft" style="padding-top: 35px;">
						<h3><?php echo $this->config->item('company'); ?></h3>
						<p><?php echo nl2br($this->config->item('address')); ?><br><?php echo $this->config->item('phone'); ?></p>
					</div>
					<?php } ?>
					<?php
					if($this->config->item('company_logo') != '')
					{
					?>
					<div class="col-md-6 fleft">
						<img src="company_logo.png">
					</div>
					<?php } ?>
				</div>
				<div class="col-md-12 fleft" style="margin-top: 50px;">
					<table class="details">
						<tbody>
							<tr>
								<th style="width:30%">From</th>
								<?php
								if(isset($customer))
								{
								?>
								<th style="width: 40%">Bill To</th>
								<?php } ?>
								<th>Receipt #</th>
								<td><?php echo $this->lang->line('sales_id').": ".$sale_id; ?></td>
							</tr>
							<tr>
								<td><?php echo $this->lang->line('employees_employee').": ".$employee; ?></td>
								<?php
								if(isset($customer))
								{
								?>
								<td><?php echo $this->lang->line('customers_customer').": ".$customer; ?><br>
								Customer Number : <?php echo $phone_number; } ?></td>
								<th>Receipt Date</th>
								<td><?php echo $transaction_time ?></td>
							</tr>
						</tbody>
					</table>
					<hr class="hr">
				</div>
			</div>
			<div class="items col-md-12 fleft" style="padding:0 30px 0 30px">
				<table class="receipt">
					<tbody>
						<thead>
							<tr class="head">
								<th style="width: 40%;">Items</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody class="rec-tbody">
							<?php
							foreach($cart as $line=>$item){
								if($item['print_option'] == PRINT_YES)
								{
								?>
									<tr>
										<td><?php echo ucfirst($item['name']); ?></td>
										<td><?php echo to_currency($item['price']); ?></td>
										<td><?php echo to_quantity_decimals($item['quantity']); ?></td>
										<td class="total-value"><?php echo to_currency($item[($this->config->item('receipt_show_total_discount') ? 'total' : 'discounted_total')]); ?></td>
									</tr>
									<tr>
										<?php
										if($this->config->item('receipt_show_description'))
										{
										?>
											<td colspan="2"><?php echo $item['description']; ?></td>
										<?php
										}

										if($this->config->item('receipt_show_serialnumber'))
										{
										?>
											<td><?php echo $item['serialnumber']; ?></td>
										<?php
										}
										?>
										<td></td>
									</tr>
									<?php
									if($item['discount'] > 0)
									{
									?>
										<tr>
											<td colspan="3"
												class="discount"><?php echo number_format($item['discount'], 0) . " " . $this->lang->line("sales_discount_included") ?></td>
											<td class="total-value"><?php echo to_currency($item['discounted_total']); ?></td>
										</tr>
									<?php
									}
								}
							}
							?>
							
						</tbody>
					</tbody>
				</table>
				<h2 class="thank-y pull-right">Thank You</h2>
			</div>
		</div>
