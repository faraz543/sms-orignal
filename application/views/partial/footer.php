		</div>
	</div>

	<div id="footer">
		<div class="jumbotron push-spaces">
			<strong><?php echo $this->lang->line('common_you_are_using_ospos'); ?>
  			</strong>.
			<?php echo $this->lang->line('common_please_visit_my'); ?>
			<h3 style="margin-top: 14px;">Powered By Faujeeks</h3>
			<a href="faujeeks.com" target="_blank"><?php echo $this->lang->line('common_website'); ?></a>
			<?php echo $this->lang->line('common_learn_about_project'); ?>
			
		</div>
	</div>
	<?php if($_SERVER[REQUEST_URI] == '/sales' ||
 		explode('/sales', $_SERVER[REQUEST_URI])[1] != '' ||
  		$_SERVER[REQUEST_URI] == '/receivings' ||
   		explode('/receivings', $_SERVER[REQUEST_URI])[1] != ''){ ?>
	<script type="text/javascript">
		var price_field = $("[name=price]");
		price_field.each(function(){
			$(this).hide();
			val = $(this).attr('value');
			$(this).parent().append("<button class='btn btn-primary' style='padding:5px 15px 4px 15px;background-color:#3e3f3a;'>"+val+"</button>");
		})
	</script>
	<?php } ?>
</body>
</html>
